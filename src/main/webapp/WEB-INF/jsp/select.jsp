<%--
  Created by IntelliJ IDEA.
  User: ASUS
  Date: 2021/5/27
  Time: 17:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>查询全部</title>
</head>
<body>
<div>

    <table border="1">
        <tr>
            <td colspan="7">人员列表</td>
        </tr>
        <tr>
            <td>编号</td>
            <td>姓名</td>
            <td>性别</td>
            <td>年龄</td>
            <td>等级</td>
            <td>部门</td>
            <td>操作</td>
        </tr>
        <c:forEach var="prs" items="${allPerson}">
            <tr>
                <td>${prs.id}</td>
                <td>${prs.name}</td>
                <td>${prs.sex}</td>
                <td>${prs.age}</td>
                <td>${prs.rank}</td>
                <td>${prs.department}</td>
                <td><a href="javascript:void(0)" onclick="de(this,${prs.id})">删除</a></td>
            </tr>
        </c:forEach>
    </table>
    <form method="post" action="add1">
        <button>添加人员</button>
    </form>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/statics/js/jquery-1.12.4.js"></script>
<script>
    function de(td, id) {
        var aa = confirm("是否删除?");
        if (aa == true) {
            $.ajax({
                type: "POST",
                url: "delete",
                data: "id=" + id,
                dataType: "json",
                success: function (data) {
                    $(td).parent().parent().remove();
                    alert("删除成功");
                },
                error: function (data) {
                    alert("失败")
                }
            });
        } else {
            alert("取消删除");
        }
    }
</script>
</body>
</html>
