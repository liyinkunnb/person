package cn.kgc.demo1.pojo;


public class Person {

    private long id;
    private String name;
    private String sex;
    private long age;
    private String rank;
    private String department;

    public Person() {
    }

    public Person(long id, String name, String sex, long age, String rank, String department) {
        this.id = id;
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.rank = rank;
        this.department = department;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }


    public long getAge() {
        return age;
    }

    public void setAge(long age) {
        this.age = age;
    }


    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }


    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

}
