package cn.kgc.demo1.mapper;

import cn.kgc.demo1.pojo.Person;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * ---------- @ClassName :    PersionMapper
 * <p>
 * ---------- @Author : 谦谦君子刘庚鑫
 * <p>
 * ----------@Date:  2021/5/27     17:30
 * <p>
 * ----------@Description :
 */

public interface PersonMapper {
    /*倒序查询所有人*/
    List<Person> getAllPerson();

    /*添加人*/
    Integer addPerson(Person person);

    /*根据id删除人*/
    Integer deletePersonById(@Param("id") Integer id);
}
