package cn.kgc.demo1.controller;

import cn.kgc.demo1.pojo.Person;
import cn.kgc.demo1.service.PersonService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Controller
public class PersonController {
    @Resource
    PersonService personService;

    @RequestMapping("index")
    public String select(Model model) {
        List<Person> allPerson = personService.getAllPerson();
        model.addAttribute("allPerson", allPerson);
        return "select";
    }

    @RequestMapping("add1")
    public String insert() {
        return "add";
    }

    @RequestMapping("add")
    public String add(Person person) {
        Integer insertperson = personService.addPerson(person);
        return "redirect:index";
    }

    @RequestMapping("delete")
    @ResponseBody
    public Integer del(Integer id) {
        Integer delete = personService.deletePersonById(id);
        return delete;

    }
}
