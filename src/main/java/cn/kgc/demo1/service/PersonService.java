package cn.kgc.demo1.service;

import cn.kgc.demo1.pojo.Person;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PersonService {
    /*倒序查询所有人*/
    List<Person> getAllPerson();

    /*添加人*/
    Integer addPerson(Person person);

    /*根据id删除人*/
    Integer deletePersonById( Integer id);
}
