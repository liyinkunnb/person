package cn.kgc.demo1.service;

import cn.kgc.demo1.mapper.PersonMapper;
import cn.kgc.demo1.pojo.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonServiceImpl implements PersonService{

    @Autowired
    PersonMapper personMapper;

    @Override
    public List<Person> getAllPerson() {
        return personMapper.getAllPerson();
    }

    @Override
    public Integer addPerson(Person person) {
        return personMapper.addPerson(person);
    }

    @Override
    public Integer deletePersonById(Integer id) {
        return personMapper.deletePersonById(id);
    }
}
